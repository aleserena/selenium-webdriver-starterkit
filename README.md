# selenium-webdriver-starterkit

## Description

This is a short "guide" on how to start with selenium webdriver mainly focused to test the tests (cuack) exported from GhostInspector

## What i've done (and what should you do)

1. Install Selenium Webdriver
...
npm install selenium-webdriver
...

2. Copy WebDrivers from /bin to your system PATH and extract the correct version of chrome and firefox webdriver

3. Run a test as an example
...
node Examples/GItest.js
...

## Further reading (things I should add)

https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Your_own_automation_environment

